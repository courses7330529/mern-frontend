import React from 'react'
import { Outlet } from 'react-router-dom'

//дава възможност за глобал банери и др.
const Layout = () => {
  return <Outlet />
}

export default Layout 